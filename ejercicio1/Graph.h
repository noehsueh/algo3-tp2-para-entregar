//
// Created by Noé Fabián HSUEH on 07/05/2022.
//

#ifndef BORRADOR_TP_GRAPH_H
#define BORRADOR_TP_GRAPH_H

#include <iostream>
#include <vector>
#include <list>
#include <fstream>
#include <string>


using namespace std;
using nodo = int;



class Graph {
public:
    Graph(string);
    int cant_nodos() const;
    int cant_aristas() const;



    int esGeodesico();
    /*
     * esGeodesico() determina si el grafo G es geodésico
     */



private:
    struct arista{
        arista(nodo x, nodo y) {
            make_pair(x,y);
            _x = min(x,y);
            _y = max(x,y);
        }

        bool enArbol(const vector<int>& PI) {
            bool res = true;
            res = PI[_y]==_x || PI[_x] == _y;
            return res;
        }
        nodo x()const{ return _x;};
        nodo y()const{ return _y;};
        nodo _x;
        nodo _y;
    };

    int _N;
    int _M;
    vector<list<nodo>> _adj;
    list<arista> _listaArista;


    vector<int> BFS(nodo s, vector<int>& d);
    /*
     * BFS enraizado en el nodo s
     * input: Grafo-> This
     * output: árbol BFS representado por vector de tamaño _N, donde en la pos j tiene el
     * predecesor del nodo j ésimo en el camino desde s hasta j
     */

    bool es_vGeodesico(nodo, vector<nodo>&, vector<nodo>&);
    /*
     * es_vGeodesico dado un nodo v devuelve si es v-Geodesico
     * si lo es, devuelve en res un vector de predecesores, donde en la pos i
     * del vector res indica el predecesor del nodo i
     * si no lo es, devuelve dos caminos que permiten llegar desde v
     */
    void addEdge(int, int);
    vector<nodo> camino(nodo, nodo, vector<nodo>&);
    void mostrarVector(vector<nodo> &vec);
};


#endif //BORRADOR_TP_GRAPH_H
