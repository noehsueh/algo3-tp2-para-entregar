//
// Created by chech on 07/05/2022.
//

#ifndef TP2_EJERCICIO2_H
#define TP2_EJERCICIO2_H
#include <vector>
#include <iostream>
#include <fstream>
using namespace std;

//Funciones
int find(struct subset subsets[], int i);
void Union(struct subset subsets[], int x, int y, int &count);
vector<int> devolverFila(string ruta, int i);
int cantidadConexas(string ruta);



#endif //TP2_EJERCICIO2_H
