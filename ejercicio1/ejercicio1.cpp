#include <iostream>
#include <iomanip>
#include "Graph.h"

// main pra usarse desde clion

//int main() {
////    for (int i = 0; i < 6; ++i) {
////        string file;
//////        file = "../no_geodesicas/instancia" + to_string(i) + ".txt";
////        file = "../geodesicas/instancia_" + to_string(i) + ".txt";
////        Graph G(file);
//////        cout << file << endl;
//////        bool res = G.esGeodesico();
//////        cout << res << endl;
////    }
//}

//// main para usarse desde la consola
int main(int argc, char** argv) {
    Graph G(argv[1]);
    G.esGeodesico();
    return 0;
}