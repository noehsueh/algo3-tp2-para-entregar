//
// Created by Noé Fabián HSUEH on 07/05/2022.
//

#include "Graph.h"

Graph::Graph(string archivo) {
    ifstream entrada(archivo.c_str());
    if(entrada.fail()){
        cout << "Error en archivo " << archivo << endl;
        exit(0);
    }

    entrada >> _N >> _M;
    _adj = vector<list<nodo>> (_N);
    for (int i = 0; i < _M; ++i) {
        int v, w;
        entrada >> v >> w;
        addEdge(v,w);
    }
}
int Graph::cant_nodos() const {
    return _N;
}
int Graph::cant_aristas() const {
    return _M;
}

void Graph::addEdge(int v, int w) {
    _adj[v].push_back(w);
    _adj[w].push_back(v);
    arista e(v, w);
    _listaArista.push_back(e);
}

vector<int> Graph::BFS(nodo s, vector<int>& d) {
    int N = cant_nodos();
    vector<int>  padres(N, N+1);
    vector<int>  distancias(N, N+1);
    list<nodo> queue;

    queue.push_back(s);
    padres[s] = s;
    distancias[s] = 0;

    while (!queue.empty()){
        nodo u = queue.front();
        queue.pop_front();
        for ( auto v : _adj[u]) {
            if(padres[v] == N+1){
                padres[v] = u;
                distancias[v] = distancias[u]+1;
                queue.push_back(v);
            }
        }
    }
    d = distancias;
    return padres;
}

int Graph::esGeodesico() {
    int N = cant_nodos();
    list<vector<nodo>> MAT_predecesores;
    for (int i = 0; i < cant_nodos(); ++i) {
        vector<nodo> res, otroCamino;
        if (!es_vGeodesico(i, res, otroCamino)){
            cout << 0 << endl;
            mostrarVector(res);
            mostrarVector(otroCamino);
            return 0;
        }
        MAT_predecesores.push_back(res);
    }
    cout << 1 << endl;
    for (auto fila : MAT_predecesores) {
        mostrarVector(fila);
    }
    return 1;
}

bool Graph::es_vGeodesico(nodo v, vector<nodo>& res, vector<nodo>& otroCamino) {
    vector<int> dist_v;
    vector<int> PI = BFS(v, dist_v);
    for(auto e : _listaArista){
        if (!e.enArbol(PI)){
            // me fijo si la arista e=(x,y) conectan nodos en niveles consecutivos
//            bool b = dist_v[e.desde()] == dist_v[e.hasta()] + 1 || dist_v[e.hasta()]== dist_v[e.desde()]+1;
//            bool b= (abs(dist_v[e.desde()]- dist_v[e.hasta()]) ==1) ;
            if (abs(dist_v[e.x()]- dist_v[e.y()]) ==1){ // encontre dos caminos minimos diferentes desde v hasta y
                nodo x = e.x();
                nodo y = e.y();
                if(dist_v[e.x()] < dist_v[e.y()]){
                    x = e.y();
                    y = e.x();
                }
                res = camino(v, x, PI); // camino dado por el árbol BFS
                otroCamino = camino(v, y, PI);
                otroCamino.push_back(x);
                return false;
            }
        }
    }
    res = PI;
    return true;
}

void Graph::mostrarVector(vector<nodo> &vec) {
    for (auto u:vec) {
        cout << right << setw(2) << u << " ";
//        cout << u << " ";
    }
    cout << endl;
}

vector<nodo> Graph::camino(nodo s, nodo t, vector<nodo>& PI) {
    vector<nodo> res;
    res.push_back(t);
    nodo v = t;
    while (PI[v] != s){
        res.push_back(PI[v]);
        v = PI[v];
    }
    res.push_back(s);
    reverse(res.begin(), res.end());
    return res;
}


