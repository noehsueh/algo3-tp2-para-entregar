
#include <vector>
#include "fstream"
#include "ejercicio2.h"

using namespace std;

/*Informacion de cada subconjunto*/

struct subset {
    int parent;
    int rank;
};

/* Funcion para encontrar a que conjunto pertenece i
usando path comprehension */
int find(struct subset subsets[], int i){
    if (subsets[i].parent != i) subsets[i].parent = find(subsets, subsets[i].parent);
    return subsets[i].parent;
}

/* Funcion que une dos conjuntos x e y
usando union by rank */
void Union(struct subset subsets[], int x, int y, int &count){
    count = count-1;
    if (subsets[x].rank < subsets[y].rank){
        subsets[x].parent = y;
    } else if (subsets[x].rank > subsets[y].rank){
        subsets[y].parent = x;
    }
    else {
        subsets[y].parent = x;
        subsets[x].rank++;
    }
}

//Devuelve la fila i de la matriz 
vector<int> devolverFila(string ruta,int i){
    ifstream file;
    file.open(ruta);
    int m,n;       
    file >> m >> n;
    int c;
    vector<int> res(n, 0);
    for (int h = 0; h < m; ++h) {
        for (int j = 0; j < n; ++j) {
            file >> c;
            if(h == i){
               res[j] = c;
            }
        }
    }
    file.close();
    return res;
}


/*Funcion que devuelve la cant de componentes conexas*/
int cantidadConexas(string ruta){
    ifstream file;
    file.open(ruta);
    int m,n;       
    file >> m >> n; //filas, columnas
    int count_ = 0;
    
    // Pedir lugar para construir nm sets
    struct subset* subsets = (struct subset*) malloc (n*m * sizeof(struct subset));

    //initializeSubsets( subsets,file,count_);
    int c;
    while(!file.eof()){
        for (int i = 0; i < m; ++i) {
            for (int j = 0; j < n; ++j) {
                file >> c;
                if (c == 1){
                    subsets[i*n+j].parent = i*n+j;
                    subsets[i*n+j].rank = 0;
                    count_++;
                }
            }
        }
    }
    file.close();

    vector<int> fanterior = devolverFila(ruta, 0);
    vector<int> factual = fanterior;
    
    for (int i = 0; i < factual.size() - 1; ++i) {
        int x = factual[i];
        int y = factual[i + 1];
        if ((x == 1 && y == 1) && (find(subsets,i+1) != find(subsets,i))){
            Union(subsets, i, i+1,count_);
        }
    }
    for (int i = 1; i < m; ++i) {
        factual = devolverFila(ruta, i);
        for (int j = 0; j < factual.size(); ++j) {
            int x = fanterior[j];
            int y = factual[j];
            int z = 0;
            if (j != 0) z = factual[j - 1];

            if ((z == 1 && y == 1) && (find(subsets,i*n+j-1) != find(subsets,i*n+j))){
                Union(subsets,i*n+j-1, i*n+j,count_);
            }

            if( (x == 1 && y == 1) && (find(subsets,i*n+j) != find(subsets,(i-1)*n+j) )){
                Union(subsets, (i-1)*n+j,i*n+j,count_);
            }
        }
        fanterior = factual;
    }
    return count_;
}

int main(int argc, char** argv) {
    cout<< cantidadConexas(argv[1])<<endl;
    return 0;
}


