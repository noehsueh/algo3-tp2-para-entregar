#include <iostream>
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <set>
#include <map>
using namespace std;

vector<vector<vector<int>>> leerArchivoGrafoListaAdyacencia(string nombre);
bool johnson(vector<vector<vector<int>>> &grafo, vector<vector<pair<int, int>>> &M);

template<typename tipo>
void visualizarVector(const vector<tipo> v){ // Da una manera facil de ver las distancias desde un nodo. (que es un vector de pares distancia, padre)
    for(int i=0;i<v.size();i++){
        if(v[i].first<114748364) { // Este valor es considerado infinito, con lo cual si es menor se imprime y sino se escribe INF
            std::cout << v[i].first << " ";
        }
        else{
            std::cout << "INF " ;
        }
    }
    std :: cout << " "<< endl ;
}
template<typename tipo>
void visualizarMatrizPares(const vector<vector<tipo>> m){ // Dada una matriz de pares distancia, padre. Te la imprime las distancias al llamar N veces al visualizar fila.
    for(int i=0;i<m.size();i++){
        visualizarVector(m[i]);
    }
    std :: cout << " "<< endl ;
}

//#include "ejercicio3.cpp"

int main(int argc, char** argv) {
    vector<vector<vector<int>>> grafo;
    grafo = leerArchivoGrafoListaAdyacencia(argv[1]);
    vector<vector<pair<int, int>>> M(grafo.size()); // Debo antes de llamar a johnson crear la matriz donde guardare distancias y caminos.
    if(johnson(grafo, M)){
        cout << "1" << endl; // Formato en caso positivo
        visualizarMatrizPares(M); // Formato en caso positivo
    }    // formato en caso negativo se hace en otra funcion de manera automatica, despreocuparse.
    return 0;
}
