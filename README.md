# Trabajo práctico 2 de Algoritmos III 


## Instrucciones de compilación 

### Ejercicio 1

```bash
g++ -std=c++11 Graph.cpp ejercicio1.cpp -o ej1

for i in geodesicas/*; 
 	do  
 		echo "Procesando $i"; 
 		./ej1 $i;
 	done
 
for i in no_geodesicas/*; 
 	do  
 		echo "Procesando $i"; 
 		./ej1 $i;
 	done 
 
```

### Ejercicio 2
```bash
g++ -std=c++11 ejercicio2.cpp -o ej2

for i in test/*; 
 	do  
 		echo "Procesando $i"; 
 		./ej2 $i;
 	done
 
```



### Ejercicio 3
```bash
g++ -std=c++11 ejercicio3.cpp main.cpp -o ej3

for i in test/*; 
 	do  
 		echo "Procesando $i"; 
 		./ej3 $i;
 	done
 
```




### Ejercicio 4
```bash
g++ -std=c++11 Bertossi.cpp -o bertossi

for i in instancias/*; 
 	do  
 		echo "Procesando $i"; 
 		./bertossi $i;
 	done

```



