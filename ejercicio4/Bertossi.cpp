#include "Bertossi.h"
#include <algorithm>
#include <list>
#include "Dijkstra.h"
#include <iostream>
#include <vector>
using namespace std;


void leerArchivo(string archivo, vector<intervalo>& v){
    ifstream entrada(archivo.c_str());
    if(entrada.fail()){
        cout << "Error en archivo " << archivo << endl;
        exit(0);
    }
    int n;
    entrada >> n;
    for (int i = 0; i < n; ++i) {
        int a,b;
        entrada >> a >> b;
        v.push_back(make_pair(make_pair(a,b), i));
    }
}



void bertossi(vector<intervalo>& v){

    //Ordeno vector de intervalos
    sort(v.begin(), v.end());

    //Marco los intervalos contenidos estrictamente en otros
    int nv = v.size();
    vector<int> w (nv,1);
    for (int i = 0; i < nv; ++i) {
        int bi = v[i].first.second;
        if (w[i] == 1) for (int j = i+1; j < nv && v[j].first.first < bi; ++j) {
            if (v[j].first.second < bi) {
                w[j] = -1;
            }
        }
    }

    //Determino quienes son los nodos
    int n = 0;
    for (int i = 0; i < nv; ++i) {
        if (w[i] == 1) {
            w[i] = n+1;
            n++;
        }
    }

    //Pregunto si hay un intervalo que contenga a todos (es decir, si n=1)
    if(n == 1){
        for (int i = 0; i < nv; ++i) {
            if (w[i] == 1){
                int m = v[i].second;
                cout<<2<<endl<<m<<" "<<m + (m == 0) - (m > 0)<<endl;
                return;
            }
        }
    }

    //Inicializo la matriz adyacencia
    vector<vector<int>> mAdy (2*n + 2, vector<int>(2*n + 2, -1));

    //Agrego los arcos de vin a vout con peso 0
    for (int i = 0; i < n; ++i) {
        mAdy[i+1][i+n+2] = 0;
    }

    //Agrego las aristas B y C con peso 1
    for (int i = 0; i < nv; ++i) {
        if (w[i] != -1) {
            int hasta = 2 * nv + 1;
            for (int j = i + 1; j < nv  && v[j].first.first < hasta; ++j) {
                if (v[j].first.first < v[i].first.second){
                    if (w[j] != -1) mAdy[w[i] + n + 1][w[j]] = 1;
                } else {
                    if (w[j] != -1)
                        mAdy[w[i]][w[j] + n + 1] = 1;
                    if (v[j].first.second < hasta)
                        hasta = v[j].first.second;
                }
            }
        }
    }

    // Agrego las aristas de 0 a los que se debe con peso 0
    int hasta = 2 * nv + 1;
    for (int i = 0; i < nv && v[i].first.first < hasta; ++i) {
        if (w[i] != -1)
            mAdy[0][w[i]+n+1] = 0;
        if (v[i].first.second < hasta)
            hasta = v[i].first.second;
    }

    //Agrego las aristas de los que se debe a n+1 con peso 1
    int desde = v[nv-1].first.first;
    for (int i = nv-1; i > -1; --i) {
        if (w[i] != -1 && desde < v[i].first.second)
            mAdy[w[i]][n+1] = 1;
    }
/*
    //Imprimo la matriz solo para chequear
    for (int i = 0; i < mAdy.size(); ++i) {
        for (int j = 0; j < mAdy[i].size(); ++j) {
            cout<<mAdy[i][j]<<", ";
        }
        cout<<endl;
    }
*/
    //Hago Dijkstra en el grafo
    vector<vector<vector<int>>> lAdy (2*n+2);
    vector<pair<int,int>> s (2*n+2);
    obtenerListaAdyDesdeMatrizAdy(mAdy, lAdy);
    dijkstra(lAdy, s, 0);
/*
    //Imprimo S solo para chequear
    cout<<endl<<"S: "<<endl;
    for (int i = 0; i < 2*n+2; ++i) {
        cout<<i<<" long: "<<s[i].first<<" padre: "<<s[i].second<<endl;
    }

    //Imprimo cuantos "nodos" habia en el grafo solo para chequear
    cout<<n<<endl;

    //Imprimo la lista de ady solo para chequear
    for (int i = 0; i < lAdy.size(); ++i) {
        for (int j = 0; j < lAdy[i].size(); ++j) {
            cout<< "{";
            for (int k = 0; k < lAdy[i][j].size(); ++k) {
                cout<<lAdy[i][j][k]<<", ";
            }
            cout<< "}";
        }
        cout<<endl;
    }
*/
    //Imprimo # del conj dominante minimo
    cout<<s[n+1].first<<endl;

    //Reconstruyo e imprimo el vector solucion
    vector<int> sol = reconstruirSoluBertossi(s, v, w);
    for (int i = 0; i < sol.size(); ++i) {
        cout<<sol[i]<<" ";
    }
    cout<<endl;
}



vector<int> reconstruirSoluBertossi(vector<pair<int, int>>& s, vector<intervalo>& v, vector<int>& w){
    int nv = v.size(), n = s.size()/2 - 1;
    vector<int> sol;
    list<int> aux;
    int i = s[n+1].second;
    while (i != 0){
        aux.push_front(i);
        i = s[i].second;
    }
    list<int> :: iterator it = aux.begin();
    while (it != aux.end()){
        if (next(it) != aux.end() && *it == *next(it) - n - 1){
            aux.erase(next(it));
        }
        if (*it > n)
            *it = *it - n - 1;
        sol.push_back(*it);
        ++it;
    }
    int m = sol.size();
    for (int j = 0; j < m; ++j) {
        bool listo = false;
        for (int k = 0; k < nv && !listo; ++k) {
            if (w[k] == sol[j]) {
                sol[j] = v[k].second;
                listo = true;
            }
        }
    }
    return sol;
}

int main(int argc, char** argv) {
    vector<intervalo> v;
    leerArchivo(argv[1],v);
    bertossi(v);
    return 0;
}
