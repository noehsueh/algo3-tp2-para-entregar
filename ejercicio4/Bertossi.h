//
// Created by juan on 17/05/22.
//

#ifndef BERTOSSI_BERTOSSI_H
#define BERTOSSI_BERTOSSI_H

#include <iostream>
#include <vector>
#include <list>
#include <fstream>
using namespace std;

typedef pair<pair<int,int>, int> intervalo;

void bertossi(vector<intervalo>& v);

void leerArchivo(string, vector<intervalo> &);

vector<int> reconstruirSoluBertossi(vector<pair<int, int>>& s, vector<intervalo>& v, vector<int>& w);

#endif //BERTOSSI_BERTOSSI_H
